# escooter modifikace

## Situace

Dostala se mi do ruky stará elektro koloběžka/scooter pochybné kvality i původu. A aby byla použitelná na víc, než jen "kolem komína", rozhodl jsem se ji modifikovat.
[Podobný model koloběžky](https://www.origomoto.cz/p/aero48), který není sice shodný, ale jde o dost podobnou specifikaci. V případě nedostupnosti je minimální základ [zde](archiv/escooter-model);

### Původní parametry

motor: 1kW kartáčový
baterie: 48V/12Ah PbLo (Avacom WP12-12E)
hmotnost: cca 40kg (včetně baterií)

## Provedené modifikace

### Bezkartáčový pohon

Nejjednodušší se jevilo pořídit celý kit z Aliexpressu.
Obsah kitu:
- motor 48V/2000W
- řídící jednotka
- zámek s klíčkem
- plyn s přepínači
- řetěz
- rozeta zadního kola

Kit měl podle prodejce být schopný jet dopředu i dozadu ve 3 rychlostech, což se mi nepodařilo zprovoznit a protože jsem toto neplánoval využívat, přestal jsem plýtvat energií na zprovoznění.
[Mnou pořízený kit](https://www.aliexpress.com/item/1005003086555927.html) (V případě je nejdůležitější výtah inzerátu archivován [zde](archiv/BLDC-kit).

### Demontáž sedadla

Byť se tahle modifikace může zdát jako zbytečná, demontáží sedadla se ušetří značná váha, která je při maximalizaci dojezdu podstatná.

Sedadlo je už od výroby sundavací, takže stačilo jej jen sundat a ponechat montážní otvor prázdný. Do budoucna by bylo nejlepší zbavit se i montážní díry, protože trčí z platformy nahoru.

### Li-ion baterie

Vzhledem k "úmrtí" starých PbLo článků, které se projevilo nepříjemným dojezdem 5km, a nutnosti vynést 16kg baterii několik pater do bytu pro každé nabití se stala Li-ion baterie velmi rychle prioritou. Nakonec jsem se rozhodl kvůli ceně a dostupnosti pro firmu Phylion a jejich 14Ah LMO články.

O jejich ochranu a správu se bude starat Daly Smart BMS (13s/60A). Jakmile bude baterie oživená, doplním další informace, jako co třeba je schopná BMS ukládat za informace, atd.

Co se týká článků samotných, tam mě čeká ještě překontrolovat slibovanou kapacitu, vymyslet co nejlepší složení do packu a otestovat reálné chování.

## Plán

Celkový plán bude zahrnovat modifikace jak konstrukce koloběžky, tak jejího pohonu a dalších souvisejících věcí

### Seznam modifikací

Seznam se bude v čase měnit.
- [x] Demontáž sedadla
- [ ] Zadní/přední světlo
- [ ] Grip na platformu
- [ ] Li-ion baterie
    - [ ] Daly BMS 13s
    - [ ] Phylion LMO 14Ah
        - [ ] Ověřit kapacitu
- [x] Bezkartáčový pohon
- [ ] Redesign přední vidlice
- [ ] Arduino
    - [x] Směrovky
    - [x] Měření napětí baterie
    - [ ] Měření proudového odběru
        - [ ] senzor WCS1700
    - [ ] Displej
    - [ ] Napájecí obvod
