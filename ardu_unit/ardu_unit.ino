// Arduino Micro code for escooter use
// www.gitlab.com/j4n3z/escooter

#define u_read A5
#define L 2
#define R 3

#define Lo 4
#define Ro 5

float U = 0;
float Ur = 0;
float Uref = 5.0;
float pomer = 12.1;

int blinkr = 0;
bool BL = LOW;
bool BR = HIGH;

void setup() {
  Serial.begin(9600);
  pinMode(L,INPUT);
  pinMode(R,INPUT);
  pinMode(Lo,OUTPUT);
  pinMode(Ro,OUTPUT);
  delay(5000);
}

void loop() {
  Ur = analogRead(u_read);
  U = Uref/1023.0*Ur*pomer;
  Serial.println(Ur);
  Serial.println(U);
  // Blinkry
  if(digitalRead(L)==1){
    blinkr=1;
    BL=!BL;
  }
  else if(digitalRead(R)==1){
    blinkr=2;
    BR=!BR;
  }
  else{
    blinkr=0;
    BL=LOW;
    BR=LOW;
  }
  digitalWrite(Lo,BL);
  digitalWrite(Ro,BR);
  Serial.print("Blinkr: ");
  Serial.println(blinkr);
  delay(500);
}